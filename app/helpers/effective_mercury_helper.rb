module EffectiveMercuryHelper
  # def mercury_region(region, options = {}, &block)
  #   type = options.delete(:type) || :full
  #   tag = options.delete(:tag) || :div

  #   opts = {:id => region, 'data-mercury' => type.to_s}.merge(options)

  #   if request.fullpath.include?('mercury_frame') # If we need the editable div
  #     content_tag(tag, opts) do
  #       content_for?(region) ? add_snippet_divs(content_for(region)) : ((capture(&block).strip.html_safe) if block_given?)
  #     end
  #   else
  #     content_for?(region) ? content_for(region) : ((capture(&block).strip.html_safe) if block_given?)
  #   end
  # end

  # # We're finding [snippet_0/1] and expanding to
  # # <div data-snippet="snippet_0" class="text_field_tag-snippet">[snippet_0/1]</div>
  # def add_snippet_divs(html)
  #   html.scan(/\[snippet_\d+\/\d+\]/).flatten.each do |snippet|  # Find [snippet_1/1]
  #     id = snippet.scan(/\d+/).try(:first).to_i
  #     html.gsub!(snippet, "<div data-snippet='snippet_#{id}' class='#{(@page.snippets["snippet_#{id}"][:name] rescue 'page')}-snippet'>[snippet_#{id}/1]</div>")
  #   end
  #   html.html_safe
  # end
end
