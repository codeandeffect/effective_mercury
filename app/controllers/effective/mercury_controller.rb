module Effective
  class MercuryController < ApplicationController
    layout false

    def resource
      render :action => "/#{params[:type]}/#{params[:resource]}"
    end

    def snippet_options
      @options = params[:options] || {}
      render :file => "/effective/snippets/#{params[:name]}/options"
    end

    # Mercury Editor wants to preview the snippet inside a content area
    def snippet_preview
      klass = "Effective::Snippets::#{params[:name].try(:classify)}".safe_constantize

      if klass.present?
        @snippet = klass.new(params[:options])
        render :partial => @snippet.to_partial_path, :object => @snippet, :locals => {:snippet_preview => true}
      else
        render :text => "Missing class Effective::Snippets::#{params[:name].try(:classify)}"
      end
    end
  end
end
