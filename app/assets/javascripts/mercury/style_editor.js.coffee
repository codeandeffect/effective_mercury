@Mercury.styleEditor = () ->
  Mercury.styleEditor.load()
  return Mercury.styleEditor

jQuery.extend Mercury.styleEditor,

  load: () ->

  updateStyleDropdown: (select) ->
    return if Mercury.region.selection == undefined

    selection = jQuery(Mercury.region.selection().commonAncestor())
    styles = []
    tags = []

    # Collect all the unique tags in an array
    tags.push(element.tagName.toLowerCase()) for element in Mercury.region.path()
    tags = tags.unique().reverse()

    # For each tag, check if we have classes available and push a <div> onto the styles array
    for tag in tags
      for name, description of (Mercury.config.styles[tag] || {})
        if selection.closest(tag).hasClass(name)
          styles.push("<div data-command='removeStyle' data-tag='#{tag}' data-style='#{name}'>#{tag}: Remove #{description}</div>")
        else
          styles.push("<div data-command='addStyle' data-tag='#{tag}' data-style='#{name}'>#{tag}: Apply #{description}</div>")
      styles.push("<div><hr></div>") if Mercury.config.styles[tag] != undefined

    # Cleanup styles
    if styles.length == 0
      styles.push("<div>No Applicable Styles</div>")
    else
      styles.pop() # Remove the last <hr> divider

    select.html("<div class='mercury-select-options'>#{styles.join('')}</div>")

  addStyle: (tag, style) ->
    selection = jQuery(Mercury.region.selection().commonAncestor()).closest(tag)
    selection.addClass(style) unless selection.hasClass(style)

  removeStyle: (tag, style) ->
    jQuery(Mercury.region.selection().commonAncestor()).closest(tag).removeClass(style)
