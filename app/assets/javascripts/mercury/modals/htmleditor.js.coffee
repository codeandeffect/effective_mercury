@Mercury.modalHandlers.htmlEditor = ->

  # fill the text area with the content
  content = Mercury.region.content(null, true, false)
  content = $.htmlClean(content, {format: true})
  @element.find('textarea').val(content)

  # replace the contents on form submit
  @element.find('form').on 'submit', (event) =>
    event.preventDefault()
    value = @element.find('textarea').val()
    cleaned = value.replace(/\r|\n|\t/g, "")
    Mercury.trigger('action', {action: 'replaceHTML', value: cleaned})
    @hide()
