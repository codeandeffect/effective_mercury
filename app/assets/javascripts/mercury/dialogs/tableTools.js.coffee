@Mercury.dialogHandlers.tableTools = ->
  @element.find('[data-command]').on 'click', (event) =>
    command = jQuery(event.target).data('command')
    Mercury.trigger('action', {action: command})
    Mercury.trigger('focus:frame')
