@Mercury.dialogHandlers.style = ->
  @element.on 'click', '[data-command]', (event) =>
    obj = jQuery(event.target)
    Mercury.styleEditor[obj.data('command')](obj.data('tag'), obj.data('style'))

  Mercury.on "before:appear", (event, options) => Mercury.styleEditor.updateStyleDropdown(@element)
