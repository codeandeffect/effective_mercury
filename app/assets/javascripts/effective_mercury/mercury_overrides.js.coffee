jQuery(window).on 'mercury:ready', ->
  Mercury.config.regions.dataAttributes = ['regionable_type', 'regionable_id', 'title']

  snippets = new Object() # An associative array

  jQuery('#mercury_iframe').contents().find('div[data-snippet]').each ->
    obj = jQuery(this)
    parent = obj.closest('div[data-mercury]')

    snippet = obj.data('snippet')
    title = parent.data('title')
    regionable_type = parent.data('regionable_type')
    regionable_id = parent.data('regionable_id')

    snippets[snippet] = {title: title, regionable_type: regionable_type, regionable_id: regionable_id}

  if Object.keys(snippets).length && Mercury.config.snippetsUrl != undefined
    jQuery.get(
      Mercury.config.snippetsUrl, {'snippets' : snippets }
    ).done (data) -> Mercury.Snippet.load(data)

#### Save button feedback

jQuery(window).on 'mercury:saving', (data) ->
  jQuery('.mercury-save-button').addClass('saving').children('em').html('Saving...')

jQuery(window).on 'mercury:saved', (response) ->
  jQuery('.mercury-save-button').removeClass('saving').children('em').html('Saved')
  setTimeout("jQuery('.mercury-save-button > em').html('Save')", 2000)

jQuery(window).on 'mercury:save_failed', (response) ->
  jQuery('.mercury-save-button').removeClass('saving').children('em').html('Save')
