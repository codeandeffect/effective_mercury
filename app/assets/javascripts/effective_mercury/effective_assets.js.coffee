@Mercury.config.toolbars.primary.insert_effective_asset = ['Asset', 'Insert Asset', { modal: 'effective_assets.html', regions: ['full', 'markdown'] }]

@Mercury.preloadedViews['effective_assets.html'] = ("
  <div class='mercury-display-pane-container form-inputs insert-effective-assets-container'>" + 
    "<fieldset>" +
      "<legend>Assets</legend>" +
      "<div class='control-group'>" +
        "<iframe id='effective_assets_iframe' src='/effective/assets'></iframe>" + 
      "</div>" +
    "</fieldset>" +
  "</div>" + 
  "<div class='form-actions mercury-display-controls'>" + 
    "<input class='btn btn-primary' type='submit' value='Close'>" + 
  "</div>")

@Mercury.modalHandlers.insert_effective_asset = ->
  modal_dialog = this

  @element.on 'click', "input[type='submit']", (event) ->
    event.preventDefault()
    modal_dialog.hide()

  $('#effective_assets_iframe').on 'load', ->
    $(this).contents().on 'click', 'a.attachment-insert', (event) ->
      event.preventDefault()
      to_insert = $(event.currentTarget).data('asset')
      modal_dialog.hide()
      Mercury.trigger('action', {action: 'insertHTML', value: to_insert})
