# Deprecation

This gem has been deprecated and is no longer maintained.

It is based on Mercury Editor (http://jejacks0n.github.io/mercury/) HTML5 editor, which has some waning support from the original author.

We have retired this implementation in favor or effective_ckeditor.


# Effective Mercury

Wraps up the Mercury Editor Javscript library (http://jejacks0n.github.io/mercury/) for use with other effective_* gems

Not intended for use as a standalone gem.

Rails >= 3.2.x, Ruby >= 1.9.x.  Has not been tested/developed for Rails4.

## Usage

Please see effective_pages (https://github.com/code-and-effect/effective_pages)

## Mercury Editor

This gem packages only the javascript parts of the Mercury Editor 0.9.0 build.

It does not use any of the rails implementation.


## License

MIT License.  Copyright Code and Effect Inc. http://www.codeandeffect.com

You are not granted rights or licenses to the trademarks of Code and Effect

## Credits

The authors of this gem have nothing to do with the following awesome project:

Mercury Editor (http://jejacks0n.github.io/mercury/ - https://github.com/jejacks0n/mercury)
