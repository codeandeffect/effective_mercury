module EffectiveMercury
  module Generators
    class InstallGenerator < Rails::Generators::Base
      include Rails::Generators::Migration

      desc "Creates an EffectiveMercury initializer in your application."

      source_root File.expand_path("../../templates", __FILE__)

      def copy_initializer
        template "effective_mercury.rb", "config/initializers/effective_mercury.rb"
      end

      def show_readme
        readme "README" if behavior == :invoke
      end
    end
  end
end
