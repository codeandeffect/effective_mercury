require "effective_mercury/engine"
require "effective_mercury/version"

module EffectiveMercury
  mattr_accessor :styles

  def self.setup
    yield self
  end

  def self.styles_json
    (styles || {}).to_json rescue '{}'
  end
end
