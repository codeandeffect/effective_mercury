module EffectiveMercury
  class Engine < ::Rails::Engine
    engine_name 'effective_mercury'
    isolate_namespace EffectiveMercury

    # Include Helpers to base application
    initializer 'effective_mercury.action_controller' do |app|
      ActiveSupport.on_load :action_controller do
        helper EffectiveMercuryHelper
      end
    end

    # Set up our default configuration options.
    initializer "effective_mercury.defaults", :before => :load_config_initializers do |app|
      eval File.read("#{config.root}/lib/generators/templates/effective_mercury.rb")
    end

  end
end
